<?php

namespace Nitra\SlideshowBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SlideshowController extends NitraController
{
    /**
     * @Template("NitraSlideshowBundle:Slideshow:index.html.twig")
     */
    public function slideshowAction(
        $controls   = true,
        $pager      = true,
        $circular   = true,
        $auto       = true,
        $speed      = 1000,
        $autoWidth  = false,
        $responsive = false,
        $preload    = true
    )
    {
        $slides = $this->getSlides();

        return array(
            'slides'     => $slides,
            'controls'   => $controls,
            'pager'      => $pager,
            'auto'       => $auto,
            'speed'      => $speed,
            'circular'   => $circular,
            'autoWidth'  => $autoWidth,
            'responsive' => $responsive,
            'preload'    => $preload,
        );
    }

    /**
     * @return \Nitra\SlideshowBundle\Document\Slideshow[]
     */
    protected function getSlides()
    {
        $store = $this->getStore();

        return $this->getDocumentManager()->createQueryBuilder('NitraSlideshowBundle:Slideshow')
            ->field('stores.id')->equals($store['id'])
            ->field('image')
                ->exists(true)
                ->notEqual('')
            ->sort('sortOrder')
            ->getQuery()->execute();
    }
}