# SlideshowBundle

## Подключение маршрутизации

```yaml
    # app/config/routing.yml
    # ...
    nitra_slideshow:
        resource: "@NitraSlideshowBundle/Resources/config/routing.yml"
        prefix:   /
    # ...
```

## Добавление в AppKernel.php

```php
    # app/AppKernel.php

    # ...
    public function registerBundles()
    {
        $bundles = array(
            # ...
            new Nitra\SlideshowBundle\NitraSlideshowBundle(),
        );
    }
    # ...
```

## Использование в проекте

### Подключение скрипта

Подключается по умолчанию в основном шаблоне (e-commerce-site/storebundle/Nitra/StoreBundle/Resources/views/baseLayout.html.twig) в вендорах, используется во всех каруселях, дополнительного подключения не требует

### Пример добавления в шаблон

```twig
    # src/Nitra/StoreBundle/Resources/views/Home/homePage.html.twig

    # ...
    {% render controller("NitraSlideshowBundle:Slideshow:slideshow", {'pager':'true','controls':'false'}) %}
    # ...
```

### Описание настроек слайдера

Полное описание настроек: http://bxslider.com/options

Доступные настройки для слайдера передаваемые в контроллер: 

* pause - интервал показа слайдов integer, по умолчанию 2000
* controls - кнопки для перелистывания (предыдущий, следущий) boolean (true / false), по умолчанию true
* autoControls - кнопки Start/Stop boolean (true / false), по умолчанию false
* pager - пагинация boolean (true / false), по умолчанию false
* auto - автоматическое перелистывание boolean (true / false), по умолчанию true
* mode - 'horizontal', 'vertical', 'fade', по умолчанию horizontal
* speed - скорость integer, по умолчанию 2000
* slideMargin - отступ между слайдами integer, по умолчанию 0

При необходимости использования дополнительных настроек необходимо переопределить бандл и блок инициализации слайдера в шаблоне