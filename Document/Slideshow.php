<?php

namespace Nitra\SlideshowBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections  as Collections;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 */
class Slideshow
{

      /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    private $name;

    /**
     * @MongoDB\String
     */
    private $description;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\Length(max = 255)
     */
    private $link;
 
    /**
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    private $image;

    /**
     * @MongoDB\Int
     * 
     */
    private $sortOrder;
    
    /**
     * @MongoDB\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    private $stores;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Slideshow
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return \Slideshow
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Get link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return \Slideshow
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set sortOrder
     *
     * @param int $sortOrder
     * @return \Slideshow
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
    public function __construct()
    {
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add stores
     *
     * @param Nitra\StoreBundle\Document\Store $stores
     */
    public function addStores(\Nitra\StoreBundle\Document\Store $stores)
    {
        $this->stores[] = $stores;
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores()
    {
        return $this->stores;
    }
    
        /**
     * Set stores
     *
     * @param Doctrine\Common\Collections\Collection $stores
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
}
